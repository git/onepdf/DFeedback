//-------------------------------------------------------------------------------------------------
// Copyright (c) 2008 DaisyDisk Team: <http://www.daisydiskapp.com>
// Some rights reserved: <http://opensource.org/licenses/mit-license.php>
//-------------------------------------------------------------------------------------------------

#import "DFFeedbackSender.h"
#import "DFFeedbackSenderDelegate.h"

//-------------------------------------------------------------------------------------------------
@implementation DFFeedbackSender
{
	BOOL _isCanceled;
	NSURLConnection* _connection;
}

//-------------------------------------------------------------------------------------------------
- (id)init
{
	self = [super init];
	if (self != nil)
	{
	}
	return self;
}

//-------------------------------------------------------------------------------------------------
- (void)dealloc
{
	[_connection release];
	[super dealloc];
}

//-------------------------------------------------------------------------------------------------
- (void)sendFeedbackToUrl:(NSString*)url
			 feedbackText:(NSString*)feedbackText 
			 feedbackType:(NSString*)feedbackType
			systemProfile:(NSString*)systemProfile
				userEmail:(NSString*)userEmail
				 
{
	// create dictionary of fields to be transmitted using http POST
    NSDictionary* form = @{@"feedbackType": feedbackType,
                           @"feedback": feedbackText,
                           //@"name": NSFullUserName(),
                           @"email": userEmail != nil ? userEmail : @"<email suppressed>",
                           @"appName": [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"],
                           @"bundleID": [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIdentifier"],
                           @"version": [NSString stringWithFormat:@"%@ build %@",
                                        [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"],
                                        [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]
                                        ],
                           @"systemProfile": systemProfile != nil ? systemProfile : @"<system profile suppressed>"};
	// create request
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPMethod: @"POST"];
    NSMutableString *tempReqBody = [@"" mutableCopy];
    for (NSString* key in form.allKeys) {
        [tempReqBody appendFormat:@"%@=%@&", key, [form objectForKey:key]];
    }
    [tempReqBody deleteCharactersInRange:NSMakeRange(tempReqBody.length-1, 1)];
    NSString *reqBody = [tempReqBody stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:[reqBody dataUsingEncoding:NSUTF8StringEncoding]];
	// begin sending the data
	_connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
    [_connection scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [_connection scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSModalPanelRunLoopMode];
    [_connection start];
}

//-------------------------------------------------------------------------------------------------
- (void)connection:(NSURLConnection*)connection didFailWithError:(NSError*)error
{
	if (!_isCanceled)
	{
        [self.delegate feedbackSender:self didFinishWithError:error];
	}
}

//-------------------------------------------------------------------------------------------------
- (void)connectionDidFinishLoading:(NSURLConnection*)connection 
{
	if (!_isCanceled)
	{
        [self.delegate feedbackSender:self didFinishWithError:nil];
	}
}

//-------------------------------------------------------------------------------------------------
- (void)cancel
{
	_isCanceled = YES;
}

@end
