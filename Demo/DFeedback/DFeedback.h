//-------------------------------------------------------------------------------------------------
// Copyright (c) 2008 DaisyDisk Team: <http://www.daisydiskapp.com>
// Some rights reserved: <http://opensource.org/licenses/mit-license.php>
//-------------------------------------------------------------------------------------------------
// DFeedback.h
//
//  DFeedback (DaisyDisk Feedback) is a two-in-one component for
//  providing user feedback:
//
//  It allows the user to send feedback and request support from
//  within your app. Along with the message, the component can
//  optionally send the user's e-mail and system configuration profile.
//
//  It automatically catches all unhandled exceptions and shows a crash
//  report window suggesting the user to send an anonymous crash report,
//  along with the stack trace, system configuration profile and optional
//  user comments.


#ifndef DFeedback_header
#define DFeedback_header

#import "DFApplication.h"
#import "DFCrashReportWindowController.h"
#import "DFFeedbackWindowController.h"

#endif
