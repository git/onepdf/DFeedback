//
//  main.m
//  relaunchdaemon
//
//  Created by Simon on 6/12/2014.
//
//

#import <Cocoa/Cocoa.h>

#pragma mark Main method
int main(int argc, char *argv[]) {
    // wait a sec, to be safe
    sleep(1);
    
    pid_t parentPID = atoi(argv[2]);
    while ([NSRunningApplication runningApplicationWithProcessIdentifier:parentPID])
        sleep(1);
    
    NSString *appPath = [NSString stringWithCString:argv[1] encoding:NSUTF8StringEncoding];
    BOOL success = [[NSWorkspace sharedWorkspace] openFile:[appPath stringByExpandingTildeInPath]];
    
    if (!success)
        NSLog(@"Error: could not relaunch application at %@", appPath);

    return (success) ? 0 : 1;
}